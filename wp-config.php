<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dotgood' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'UAP43[pk z=We3@=u}d.sSgbB1HF&ROtQ+NbodEK/<cN![|HvYL@Z&v}qV,X?EHa' );
define( 'SECURE_AUTH_KEY',  '8-}[.d}Z` BI6$[ Tc[[7HCwr6E*qlL`&AgO#Z/w|gQf4PB]Wh~Z4hfkK[*z ?1E' );
define( 'LOGGED_IN_KEY',    '1G Yyek=U7k^V*gEb#Z3@Zxc#L^@;2kDT~W?Uf8rf)& (@ 9,o>9WuG|po&[dai;' );
define( 'NONCE_KEY',        'po7?IT*0RY9DdcLDnpfAv.aV2sVhsQc?ZMU<.>gJj!e4B.ln}>INOH|LcF*m|*y.' );
define( 'AUTH_SALT',        'XIYDz.lj7mr!+)W)Fs/@Tb#M>`BPHs?$=;MqaEff~#W)0lW=NuyiG/_QaHA~esQY' );
define( 'SECURE_AUTH_SALT', '/6F6b234LGbK4#w ZrNI?5i68~= )gw^Xa@QrRn8QT;ZOZ?3h8I4iRTV?8{j;y6[' );
define( 'LOGGED_IN_SALT',   '<G`/6:zV}|lk-Bnkh3dR=38}shQv<#c<`U![u*mMCcL58.I<vY|qooZ.L8N6/dXV' );
define( 'NONCE_SALT',       '@qkeDD1Sg;02q(&[o1%UeJw<8Kq^IbF3d<eTnjhCb>pn: 4^N27[8zZ_+t590uOS' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
