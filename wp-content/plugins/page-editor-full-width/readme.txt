=== Page Editor Full Width Option ===
Plugin Name: Page Editor Full Width Option
Plugin URI: https://wordpress.org/plugins/page-editor-full-width/
Contributors: harmancheema
Tags: editor, width, gutenberg, blocks, page builder, page editor
Author URI: https://profiles.wordpress.org/harmancheema/
Author: Harmandeep Singh
Requires at least: 5.0.0
Tested up to: 5.4.2
Requires PHP: 5.2.0	
Stable tag: 1.1
Version: 1.1

This plugin adjust default width of the page editor.	

== Description ==

This WordPress plugin will change the Gutenberg editor width, which will be make it easy to use Gutenberg editor.

== Installation ==

Upload and Activate plugin. No setting required.

== SCREENSHOTS ==

1. Before plugin editor screen width
2. Full width editor screen

== Changelog ==

= 1.1 =
* Customizer seting option is removed.
* Plugin resize editor width by default activating the plugin.

= 1.0 =
* This WordPress plugin will add Gutenberg editor full width setting in customize, from where you can change the editor width on page edit screen.

== Upgrade Notice ==
= 1.1 =
* Update plugin will make plugin work automatically. Setting option is removed from the plugin.

