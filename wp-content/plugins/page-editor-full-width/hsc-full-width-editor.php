<?php 

/**
	Plugin Name: Page Editor Full Width
	Plugin URI: https://wordpress.org/plugins/page-editor-full-width/
	Description: This WordPress plugin will change the Gutenberg editor width, which will be make it easy to use Gutenberg editor.
	Author: Harmandeep Singh
	Version: 1.1
	Author URI: https://profiles.wordpress.org/harmancheema/
 */

add_action('admin_head', 'hsc_editor_width_page');
if(!function_exists('hsc_editor_width_page')){
	function hsc_editor_width_page() {
		echo '<style>
			body.page-editor-page .editor-post-title__block, body.page-editor-page .editor-default-block-appender, body.page-editor-page .editor-block-list__block {
				max-width: none !important;
			}
			.block-editor__container .wp-block {
				max-width: none !important;
			}
		</style>';
	}
}