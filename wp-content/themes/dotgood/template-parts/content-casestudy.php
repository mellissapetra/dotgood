<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dotgood
 */

?>
<?php 
	    $loop_count = 0;
	    if (has_post_thumbnail()) {
	        $loop_count =1;
	    }
	?>
	<div <?php echo $loop_count == 1 ? 'class="page_banner"' : 'class="page_banner no_banner-image"'; ?>>
		<div class="banner-top">
			 <?php if( get_field('banner_image') ): ?>
			    <img src="<?php the_field('banner_image'); ?>" />
			<?php endif; ?>
		</div>
		<div class="banner_text">
			<div class="container">
				<div class="bg_black">
					
						<h1 class="page_title">				
						<span><?php the_field('banner_text'); ?></span>					
							
					</h1>
				</div>
			</div>
		</div>
	</div>
<div class="single_blog">
	<div class="single_blog_container">
		<div class="container">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="detail-wrap">
						<?php
							if ( is_single() ) :
								//the_title( '<h2 class="single-title">', '</h2>' );
							else :
								the_title( '<h3 class="single-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
							endif;
						 ?>


					<div class="entry-content">
						<?php
							
							if( is_single() ){

								the_content( sprintf(
									/* translators: %s: Name of current post. */
									wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'wp-new-site' ), array( 'span' => array( 'class' => array() ) ) ),
									the_title( '<span class="screen-reader-text">"', '"</span>', false )
								) );
							} else{

								the_excerpt();
							}
							
						?>
					</div><!-- .entry-content -->
				</div>

			</article><!-- #post-## -->
		</div>
	</div>
		
		
	
</div>