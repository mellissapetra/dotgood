<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dotgood
 */

?>
				<div class="col-md-4">
				<div class="post-box">
					<a class="post-img" href="<?php the_permalink(); ?>">
						<?php  
							if(has_post_thumbnail()){
								the_post_thumbnail(array(275, 208));
							}else{
							echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
							}
							?>
					</a>
					<div class="main-dateformat postdate">
					<div class="date-format">
						<?php echo get_the_date('F j, Y'); ?>
					</div>
					</div>
					<div class="post-title">
							<a href="<?php the_permalink(); ?>">
								<h4><?php the_title(); ?></h4>
							</a>
					</div>
					<div class="post-text">
							<p><?php the_excerpt(); ?></p>
					</div>
					<div class="vc_btn3-container  see_more_btn vc_btn3-left">
										<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-modern vc_btn3-color-grey" href="<?php the_permalink(); ?>" title="Read More"><?php echo the_field('post_button');?></a>
					</div>
					<div class="post-author">
							<p>Posted by <?php the_author_link();?> in <?php the_category(',');?></p>
					</div>
					</div>
				</div>