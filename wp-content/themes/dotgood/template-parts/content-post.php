<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dotgood
 */

?>
<div class="single_blog">
	<div class="single_blog_container">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="detail-page-title">
					<?php the_title();?>
				    </div>
				    <div class="detail-pade-date">
				    	<div class="date-format">
						<p><?php echo get_the_date('F j, Y'); ?> | Posted By <?php the_author_link();?></p>
						</div>
				    </div>
				    <div class="detail-page-content">
				    	<?php echo get_the_content();?>
				    </div>
				</div>
				<div class="col-md-3">
						<?php dynamic_sidebar('blog-right-side')?>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="social_icon">
		    	<?php dynamic_sidebar( 'addtoany_social_icon' ); ?>
		    </div>
			<div class="row">
				<div class="col-md-6">
					<div class="previous_post_btn">
					<?php 
					echo previous_post_link( '%link', "<img src='".get_template_directory_uri()."/assets/images/dotgood-previous-button.png' />", true ); 
					?>
				    </div>
				</div>
				<div class="col-md-6">
					<div class="next_post_btn">
					<?php echo next_post_link( '%link', "<img src='".get_template_directory_uri()."/assets/images/dotgood-next-button.png' />", true ); ?>
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>