<?php 
	add_action( 'widgets_init', 'fn_register_widgets' );

		function fn_register_widgets() {

		    register_sidebar(array(
		        'name' => 'Receive our offers',
		        'id' => 'receive-our-offers',
		        'before_widget' => '<div class="widget">',
		        'after_widget' => '</div>',
		        'before_title' => '<h3 class="widget-title">',
		        'after_title' => '</h3>',
		    ));
		    register_sidebar(array(
		        'name' => 'Blog-Right-Side',
		        'id' => 'blog-right-side',
		        'before_widget' => '<div class="widget-right-title">',
		        'after_widget' => '</div>',
		        'before_title' => '',
		        'after_title' => '',
		    ));
		    register_sidebar(array(
		        'name' => 'addtoany_social_icon',
		        'id' => 'addtoany_social_icon',
		        'before_widget' => '<div class="addtoany_social_icon">',
		        'after_widget' => '</div>',
		        'before_title' => '',
		        'after_title' => '',
		    ));

		}
?>
