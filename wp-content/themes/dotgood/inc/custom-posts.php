<?php 
/*
* Add New Post (Services)
*/   
     add_action( 'init', 'fn_all_services_init' );
		
	 function fn_all_services_init() {
	 		$labels = array(
	 			'name'               => _x( 'Services', 'post type general name', 'dotgood' ),
				'singular_name'      => _x( 'Services', 'post type singular name', 'dotgood' ),
				'menu_name'          => _x( 'Services', 'admin menu', 'dotgood' ),
	 			'name_admin_bar'     => _x( 'Services', 'add new on admin bar', 'dotgood' ),
	 			'add_new'            => _x( 'Add New', 'Services', 'dotgood' ),
				'add_new_item'       => __( 'Add New Services', 'dotgood' ),
	 			'new_item'           => __( 'New Services', 'dotgood' ),
	 			'edit_item'          => __( 'Edit Services', 'dotgood' ),
				'view_item'          => __( 'View Services', 'dotgood' ),
	 			'all_items'          => __( 'All Services', 'dotgood' ),
	 			'search_items'       => __( 'Search Services', 'dotgood' ),
	 			'parent_item_colon'  => __( 'Parent Services:', 'dotgood' ),
	 			'not_found'          => __( 'No Services found.', 'dotgood' ),
	 			'not_found_in_trash' => __( 'No Services found in Trash.', 'dotgood' )
			);

			$args = array(
	 			'labels'             => $labels,
	 	        'description'        => __( 'Description.', 'dotgood' ),
	 			'public'             => true,
	 			'publicly_queryable' => true,
	 			'show_ui'            => true,
	 			'show_in_menu'       => true,								
	 			'query_var'          => true,
	 			'capability_type'    => 'post',
	 			'has_archive'        => true,
	 			'hierarchical'       => true,
	 			'menu_position'      => null,
	 			'menu_icon'           => 'dashicons-edit',
	 			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
	 		);

	 		register_post_type( 'services', $args );
	 	}
?>
<?php 
/*
* Add New Post (CaseStudy)
*/
	 add_action( 'init', 'fn_all_casestudy_init' );
		
	 function fn_all_casestudy_init() {
	 		$labels = array(
	 			'name'               => _x( 'CaseStudy', 'post type general name', 'dotgood' ),
				'singular_name'      => _x( 'CaseStudy', 'post type singular name', 'dotgood' ),
				'menu_name'          => _x( 'CaseStudy', 'admin menu', 'dotgood' ),
	 			'name_admin_bar'     => _x( 'CaseStudy', 'add new on admin bar', 'dotgood' ),
	 			'add_new'            => _x( 'Add New', 'CaseStudy', 'dotgood' ),
				'add_new_item'       => __( 'Add New CaseStudy', 'dotgood' ),
	 			'new_item'           => __( 'New CaseStudy', 'dotgood' ),
	 			'edit_item'          => __( 'Edit CaseStudy', 'dotgood' ),
				'view_item'          => __( 'View CaseStudy', 'dotgood' ),
	 			'all_items'          => __( 'All CaseStudy', 'dotgood' ),
	 			'search_items'       => __( 'Search CaseStudy', 'dotgood' ),
	 			'parent_item_colon'  => __( 'Parent CaseStudy:', 'dotgood' ),
	 			'not_found'          => __( 'No CaseStudy found.', 'dotgood' ),
	 			'not_found_in_trash' => __( 'No CaseStudy found in Trash.', 'dotgood' )
			);

			$args = array(
	 			'labels'             => $labels,
	 			'public'             => true,
	 			'publicly_queryable' => true,
	 			'show_ui'            => true,
	 			'show_in_menu'       => true,								
	 			'query_var'          => true,
	 			'capability_type'    => 'post',
	 			'has_archive'        => false,
	 			'hierarchical'       => true,
	 			'menu_position'      => null,
	 			'menu_icon'           => 'dashicons-edit',
	 			'supports'           => array( 'title','editor' ,'thumbnail' ,'excerpt', )
	 		);

	 		register_post_type( 'casestudy', $args );
	 	}
?>
<?php 
/*
* Add New Post (People)
*/
	 add_action( 'init', 'fn_all_people_init' );
		
	 function fn_all_people_init() {
	 		$labels = array(
	 			'name'               => _x( 'People', 'post type general name', 'dotgood' ),
				'singular_name'      => _x( 'People', 'post type singular name', 'dotgood' ),
				'menu_name'          => _x( 'People', 'admin menu', 'dotgood' ),
	 			'name_admin_bar'     => _x( 'People', 'add new on admin bar', 'dotgood' ),
	 			'add_new'            => _x( 'Add New', 'People', 'dotgood' ),
				'add_new_item'       => __( 'Add New People', 'dotgood' ),
	 			'new_item'           => __( 'New People', 'dotgood' ),
	 			'edit_item'          => __( 'Edit People', 'dotgood' ),
				'view_item'          => __( 'View People', 'dotgood' ),
	 			'all_items'          => __( 'All People', 'dotgood' ),
	 			'search_items'       => __( 'Search People', 'dotgood' ),
	 			'parent_item_colon'  => __( 'Parent People:', 'dotgood' ),
	 			'not_found'          => __( 'No People found.', 'dotgood' ),
	 			'not_found_in_trash' => __( 'No People found in Trash.', 'dotgood' )
			);

			$args = array(
	 			'labels'             => $labels,
	 	        'description'        => __( 'Description.', 'dotgood' ),
	 			'public'             => true,
	 			'publicly_queryable' => true,
	 			'show_ui'            => true,
	 			'show_in_menu'       => true,								
	 			'query_var'          => true,
	 			'capability_type'    => 'post',
	 			'has_archive'        => false,
	 			'hierarchical'       => true,
	 			'menu_position'      => null,
	 			'menu_icon'           => 'dashicons-edit',
	 			'supports'           => array( 'title','thumbnail','editor','excerpt')
	 		);

	 		register_post_type( 'people', $args );
	 	}
?>
<?php 
/*
* Add New Post (Brands)
*/
	 add_action( 'init', 'fn_all_brands_init' );
		
	 function fn_all_brands_init() {
	 		$labels = array(
	 			'name'               => _x( 'Brands', 'post type general name', 'dotgood' ),
				'singular_name'      => _x( 'Brands', 'post type singular name', 'dotgood' ),
				'menu_name'          => _x( 'Brands', 'admin menu', 'dotgood' ),
	 			'name_admin_bar'     => _x( 'Brands', 'add new on admin bar', 'dotgood' ),
	 			'add_new'            => _x( 'Add New', 'Brands', 'dotgood' ),
				'add_new_item'       => __( 'Add New Brands', 'dotgood' ),
	 			'new_item'           => __( 'New Brands', 'dotgood' ),
	 			'edit_item'          => __( 'Edit Brands', 'dotgood' ),
				'view_item'          => __( 'View Brands', 'dotgood' ),
	 			'all_items'          => __( 'All Brands', 'dotgood' ),
	 			'search_items'       => __( 'Search Brands', 'dotgood' ),
	 			'parent_item_colon'  => __( 'Parent Brands:', 'dotgood' ),
	 			'not_found'          => __( 'No Brands found.', 'dotgood' ),
	 			'not_found_in_trash' => __( 'No Brands found in Trash.', 'dotgood' )
			);

			$args = array(
	 			'labels'             => $labels,
	 	        'description'        => __( 'Description.', 'dotgood' ),
	 			'public'             => true,
	 			'publicly_queryable' => true,
	 			'show_ui'            => true,
	 			'show_in_menu'       => true,								
	 			'query_var'          => true,
	 			'capability_type'    => 'post',
	 			'has_archive'        => false,
	 			'hierarchical'       => true,
	 			'menu_position'      => null,
	 			'menu_icon'           => 'dashicons-edit',
	 			'supports'           => array( 'title','thumbnail')
	 		);

	 		register_post_type( 'brands', $args );
	 	}
?>

