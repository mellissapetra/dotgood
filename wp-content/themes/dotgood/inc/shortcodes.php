<?php
/*
Shortcode : [dotgood_service] dotgood_service Page 
*/
	add_shortcode('dotgood_service', 'fn_dotgood_service');
	function fn_dotgood_service($attr){ 
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
				    'posts_per_page'   => 6,
				    'post_type'     => 'services',
				    'post_status' => 'publish',
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 6;
				}
				if(isset($attr['page']) && $attr['page'] != ''){
					$args['paged'] = $attr['page'];	
					$page = $attr['page'];
				}
				if($wp_query->have_posts()):?>
					<div class="row">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<div class="col-md-4">
								<div class="service-box">
									<a class="service-img" href="<?php the_permalink(); ?>">
										<?php  
											if(has_post_thumbnail()){
												the_post_thumbnail("post_thumb");
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
									</a>
									<div class="service-title">
										<a href="<?php the_permalink(); ?>">
											<h4><?php the_title(); ?></h4>
										</a>
									</div>
									<div class="service-text">
										<p><?php the_excerpt(); ?></p>
									</div>
									<div class="vc_btn3-container  see_more_btn vc_btn3-left">
										<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-modern vc_btn3-color-grey" href="<?php the_permalink(); ?>" title="Find Out More"><?php the_field('service_button'); ?></a>
									</div>
								</div>
							</div>
						<?php endwhile;?>
					</div>
				<?php endif; ?>
			<?php wp_reset_postdata(); ?>
	<?php 
	wp_reset_postdata(); 
	$dotgood_service = ob_get_contents();
	ob_end_clean();
	return $dotgood_service;
	}
?>
<?php
/*
Shortcode : [dotgood_casestudy] dotgood_casestudy Page 
*/
	add_shortcode('dotgood_casestudy', 'fn_dotgood_casestudy');
	function fn_dotgood_casestudy($attr){ 
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
				    'posts_per_page'   => 6,
				    'post_type'     => 'casestudy',
				    'post_status' => 'publish',
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 6;
				}
				if(isset($attr['page']) && $attr['page'] != ''){
					$args['paged'] = $attr['page'];	
					$page = $attr['page'];
				}
				if($wp_query->have_posts()):?>
					<div class="row">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<div class="col-md-4">
								<div class="case_box">
									<a class="casestudy-img" href="<?php the_permalink(); ?>">
										<?php  
											if(has_post_thumbnail()){
												the_post_thumbnail(array(398, 318));
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
									</a>
									<div class="case-text-overlay">
									<div class="case-title">
										<a href="<?php the_permalink(); ?>">
											<h4><?php the_title(); ?></h4>
										</a>
									</div>
									<div class="case-text">
										<p><?php the_excerpt(); ?></p>
									</div>
									<div class="case-hover-color">
									<span style="height:320px;display:block;width:500px; background-color: <?php echo get_field('hover_color'); ?>"></span>
								    </div>
								</div>
								</div>
							</div>
						<?php endwhile;?>
					</div>
				<?php endif; ?>

			<?php wp_reset_postdata(); ?>
	<?php 
	wp_reset_postdata(); 
	$dotgood_casestudy = ob_get_contents();
	ob_end_clean();
	return $dotgood_casestudy;
	}
?>
<?php
/*
Shortcode : [dotgood_people] dotgood_people Page 
*/
	add_shortcode('dotgood_people', 'fn_dotgood_people');
	function fn_dotgood_people($attr){ 
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
				    'posts_per_page'   => -1,
				    'post_type'     => 'people',
				    'post_status' => 'publish',
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 3;
				}
				if(isset($attr['page']) && $attr['page'] != ''){
					$args['paged'] = $attr['page'];	
					$page = $attr['page'];
				}
				if($wp_query->have_posts()):?>
					<div class="row regular">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<div class="col-md-12">
								<div class="people_box">
									<div class="people-text">
										<p><?php the_excerpt(); ?></p>
									</div>
									<a class="people-img" href="<?php the_permalink(); ?>">
										<?php  
											if(has_post_thumbnail()){
												the_post_thumbnail("post_thumb");
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
											?>
									</a>
									<div class="people-title">
										<!-- <a href="<?php //the_permalink(); ?>"> -->
											<h5><?php the_title(); ?></h5>
										</a>
									</div>
									<div class="people-subtitle">
										<p><?php the_content();?></p>
									</div>
								</div>
							</div>

						<?php endwhile;?>
					</div>
				<?php endif; ?>
			<?php wp_reset_postdata(); ?>
	<?php 
	wp_reset_postdata(); 
	$dotgood_people = ob_get_contents();
	ob_end_clean();
	return $dotgood_people;
	}
?>
<?php
/*
Shortcode : [dotgood_brands] dotgood_brands Page 
*/
	add_shortcode('dotgood_brands', 'fn_dotgood_brands');
	function fn_dotgood_brands($attr){ 
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
				    'posts_per_page'   =>12,
				    'post_type'     => 'brands',
				    'post_status' => 'publish',
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 6;
				}
				if(isset($attr['page']) && $attr['page'] != ''){
					$args['paged'] = $attr['page'];	
					$page = $attr['page'];
				}
				if($wp_query->have_posts()):?>
					<div class="row">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<div class="col-md-3">
								<div class="brands-box">
									
									<a class="brand-img" href="<?php the_permalink(); ?>">
										<?php  
											if(has_post_thumbnail()){
												the_post_thumbnail("post_thumb");
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
									</a>
								</div>
							</div>
						<?php endwhile;?>
					</div>
				<?php endif; ?>
			<?php wp_reset_postdata(); ?>
	<?php 
	wp_reset_postdata(); 
	$dotgood_brands = ob_get_contents();
	ob_end_clean();
	return $dotgood_brands;
	}
?>
<?php
/*
Shortcode : [dotgood_home_post] post Page 
*/
	add_shortcode('dotgood_home_post', 'fn_dotgood_home_post');
	function fn_dotgood_home_post($attr){ 
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
				    'posts_per_page'   => 4,
				    'post_type'     => 'post',
				    'post_status' => 'publish',
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 4;
				}
				if(isset($attr['page']) && $attr['page'] != ''){
					$args['paged'] = $attr['page'];	
					$page = $attr['page'];
				}
				if($wp_query->have_posts()):?>
					<div class="row">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<div class="col-md-3">
								<div class="post-box">
									<a class="post-img" href="<?php the_permalink(); ?>">
										<?php  
											if(has_post_thumbnail()){
												the_post_thumbnail(array(275, 208));

											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
									</a>
									<div class="main-dateformat postdate">
										<div class="date-format">
										 	<?php echo get_the_date('F j, Y'); ?>
										</div>
									</div>
									<div class="post-title">
										<a href="<?php the_permalink(); ?>">
											<h4><?php the_title(); ?></h4>
										</a>
									</div>
									<div class="post-text">
										<p><?php the_excerpt(); ?></p>
									</div>
									<div class="vc_btn3-container  see_more_btn vc_btn3-left">
										<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-modern vc_btn3-color-grey" href="<?php the_permalink(); ?>" title="Read More"><?php echo the_field('post_button');?></a>
									</div>
									<div class="post-author">
										<p>Posted by <?php the_author_link();?> in <?php the_category(',');?></p>
									</div>
								</div>
							</div>
						<?php endwhile;?>
					</div>
				<?php endif; ?>
			<?php wp_reset_postdata(); ?>
	<?php 
	wp_reset_postdata(); 
	$dotgood_home_post = ob_get_contents();
	ob_end_clean();
	return $dotgood_home_post;
	}
?>
<?php
/*
Shortcode : [dotgood_ourwork_casestudy] dotgood_ourwork_casestudy Page 
*/
	add_shortcode('dotgood_ourwork_casestudy', 'fn_dotgood_ourwork_casestudy');
	function fn_dotgood_ourwork_casestudy($attr){ 
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
				    'posts_per_page'   => 9,
				    'post_type'     => 'casestudy',
				    'post_status' => 'publish',
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 6;
				}
				if(isset($attr['page']) && $attr['page'] != ''){
					$args['paged'] = $attr['page'];	
					$page = $attr['page'];
				}
				if($wp_query->have_posts()):?>
					<div class="row">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<div class="col-md-4">
								<div class="case_box">
									<a class="casestudy-img" href="<?php the_permalink(); ?>">
										<?php  
											if(has_post_thumbnail()){
												the_post_thumbnail(array(398, 318));
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
									</a>
									<div class="case-text-overlay">
									<div class="case-title">
										<a href="<?php the_permalink(); ?>">
											<h4><?php the_title(); ?></h4>
										</a>
									</div>
									<div class="case-text">
										<p><?php the_excerpt(); ?></p>
									</div>
									<div class="case-hover-color">
									<span style="height:320px;display:block;width:500px; background-color: <?php echo get_field('hover_color'); ?>"></span>
								    </div>
								</div>
								</div>
							</div>
						<?php endwhile;?>
					</div>
				<?php endif; ?>

			<?php wp_reset_postdata(); ?>
	<?php 
	wp_reset_postdata(); 
	$dotgood_ourwork_casestudy = ob_get_contents();
	ob_end_clean();
	return $dotgood_ourwork_casestudy;
	}
?>