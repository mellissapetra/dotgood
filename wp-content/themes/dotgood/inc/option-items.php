<?php 
global $option_items;
//Header Fields Starts	
$option_items[]= array(
					'type'=>'section',
					'name'=>'header',
					'title'=>'Header',
					'desc'=>'You can manage header settings from here',
				 );

$option_items[]= array(
					'type'=>'image',
					'name'=>'header-logo',
					'title'=>'Logo',
					'desc'=>'You can manage header logo from here',
				 );	

//Header Fields Ends

	
//Footer Fields Start				 
$option_items[]= array(
					'type'=>'section',
					'name'=>'footer',
					'title'=>'Footer',
					'desc'=>'You can manage footer settings from here',
				 );

$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-title',
					'title'=>'Footer Title',
					'desc'=>'You can manage footer Main Title from here',
				 );	
$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-sub-title',
					'title'=>'Footer Sub Title',
					'desc'=>'You can manage footer Sub Title from here',
				 );
$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-sub-text',
					'title'=>'Footer Sub Text',
					'desc'=>'You can manage Footer Sub Text from here',
				 );
$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-right-title-1',
					'title'=>'Footer Right Title-1',
					'desc'=>'You can manage Footer Right Title-1 from here',
				 );
$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-right-sub-title-1',
					'title'=>'Footer Right Sub Title-1',
					'desc'=>'You can manage Footer Right Sub Title-1 from here',
				 );
$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-address-1',
					'title'=>'Footer Address-1',
					'desc'=>'You can manage Footer Address-1 from here',
				 );
$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-mail-text-1',
					'title'=>'Footer Mail Text-1',
					'desc'=>'You can manage Footer Mail Text-1 from here',
				 );

$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-right-title-2',
					'title'=>'Footer Right Title-2',
					'desc'=>'You can manage Footer Right Title-2 from here',
				 );
$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-right-sub-title-2',
					'title'=>'Footer Right Sub Title-2',
					'desc'=>'You can manage Footer Right Sub Title-2 from here',
				 );
$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-address-2',
					'title'=>'Footer Address-2',
					'desc'=>'You can manage Footer Address-2 from here',
				 );

$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-mail-text-2',
					'title'=>'Footer Mail Text-2',
					'desc'=>'You can manage Footer Mail Text-2 from here',
				 );

$option_items[]= array(
					'type'=>'text',
					'name'=>'footer-bottom-text',
					'title'=>'Footer Bottom Text',
					'desc'=>'You can manage Footer Bottom Text from here',
				 );	

$option_items[]= array(
					'type'=>'image',
					'name'=>'footer-facebook',
					'title'=>'Footer Facebook',
					'desc'=>'You can manage Footer Facebook from here',
				 );	
				 
$option_items[]= array(
					'type'=>'image',
					'name'=>'footer-twitter',
					'title'=>'Footer Twitter',
					'desc'=>'You can manage Footer Twitter from here',
				 );	

$option_items[]= array(
					'type'=>'image',
					'name'=>'footer-linkedln',
					'title'=>'Footer Linkedln',
					'desc'=>'You can manage Footer Linkedln from here',
				 );	
//Footer Fields Ends			 				 			 
?>