<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dotgood
 */
 global $theme_options;
?>
    <footer class="site-footer" role="contentinfo">
		<div class="footer_top">
            <div class="container-fluid">
                <div class="row"> 
                    <div class="col-md-6">
                        <div class="footer-left-side">
                            <?php if($theme_options['footer-title'] != ''){?>
                                <h2 class="title-text-one"><?php echo $theme_options['footer-title'];?></h2>
                            <?php }?>
                            

                            <?php if($theme_options['footer-sub-title'] != ''){?>
                                <h5 class="title-text-five"><?php echo $theme_options['footer-sub-title'];?></h5>
                            <?php }?>

                            <?php if($theme_options['footer-sub-text'] != ''){?>
                                <h4 class="title-text-four"><?php echo $theme_options['footer-sub-text'];?></h5>
                            <?php }?>
                        </div>
                    </div>
                <div class="col-md-6">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="footer-right-side">
                            <?php if($theme_options['footer-right-title-1'] != ''){?>
                                <h3 class="title-text-three"><?php echo $theme_options['footer-right-title-1'];?></h3>
                            <?php }?>

                            <?php if($theme_options['footer-right-sub-title-1'] != ''){?>
                                <h4 class="title-text-four"><?php echo $theme_options['footer-right-sub-title-1'];?></h4>
                            <?php }?>

                            <?php if($theme_options['footer-address-1'] != ''){?>
                                <h4 class="title-text-four-number"><?php echo $theme_options['footer-address-1'];?></h4>
                            <?php }?>

                            <?php if($theme_options['footer-mail-text-1'] != ''){?>
                                <h4 class="title-text-four-email"><?php echo $theme_options['footer-mail-text-1'];?></h4>
                            <?php }?>
                        </div>
                    </div>
                        <div class="col-md-6">
                            <div class="footer-right-side">
                            
                            <?php if($theme_options['footer-right-title-2'] != ''){?>
                                <h3 class="title-text-three"><?php echo $theme_options['footer-right-title-2'];?></h3>
                            <?php }?>

                            <?php if($theme_options['footer-right-sub-title-2'] != ''){?>
                                <h4 class="title-text-four"><?php echo $theme_options['footer-right-sub-title-2'];?></h4>
                            <?php }?>

                            <?php if($theme_options['footer-address-2'] != ''){?>
                                <h4 class="title-text-four-number-two"><?php echo $theme_options['footer-address-2'];?></h4>
                            <?php }?>
                            
                            <?php if($theme_options['footer-mail-text-2'] != ''){?>
                                <h4 class="title-text-four-email"><?php echo $theme_options['footer-mail-text-2'];?></h4>
                            <?php }?>
                            </div>
                        </div>
                        <div class="col-md-12">
                                <hr>
                                <?php if($theme_options['footer-bottom-text'] != ''){?>
                                <h3 class="follow-social"><?php echo $theme_options['footer-bottom-text'];?></h3>
                            <?php }?>
                                    <div class="footer-icon">

                            <?php if($theme_options['footer-facebook'] != ''){?>
                                <a href="<?php echo $theme_options['footer-facebook']?>">
                                    <img class="img-fluid" src="<?php echo $theme_options['footer-facebook']?>" alt="image"/>
                                </a>
                            <?php }?>

                            <?php if($theme_options['footer-twitter'] != ''){?>
                                <a href="<?php echo $theme_options['footer-twitter']?>">
                                    <img class="img-fluid" src="<?php echo $theme_options['footer-twitter']?>" alt="image"/>
                                </a>
                            <?php }?>

                            <?php if($theme_options['footer-linkedln'] != ''){?>
                                <a href="<?php echo $theme_options['footer-linkedln']?>">
                                    <img class="img-fluid" src="<?php echo $theme_options['footer-linkedln']?>" alt="image"/>
                                </a>
                            <?php }?>
                        </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php wp_footer(); ?>


<script type="text/javascript">
 jQuery('#load_more_click').click(function(){
    jQuery.ajax({
        url:"<?php echo admin_url('admin-ajax.php'); ?>",
        methode:"get",
        data:{action:"dotgood_loadmore_blog"},
        success:function(data){
            console.log("hello");
        },
        error:function(data){
            console.log("hello");
        }
    })
 });


    $(document).on('ready', function() {
      $(".regular").slick({
        dots: false,
        autoplay:true,
        autoplaySpeed:3000,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
      });
    });
</script>
</body>
</html>