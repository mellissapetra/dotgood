<?php /* Template Name: dotgood Template*/
	global $theme_options;
	get_header(); 
 ?>
<div class="main-container">
	<?php 
	    $loop_count = 0;
	    if (has_post_thumbnail()) {
	        $loop_count =1;
	    }
	?>
	<div <?php echo $loop_count == 1 ? 'class="page_banner"' : 'class="page_banner no_banner-image"'; ?>>
		<div class="banner-top">
			 <?php if( get_field('banner_image') ): ?>
			    <img src="<?php the_field('banner_image'); ?>" />
			<?php endif; ?>
		</div>
		<div class="banner_text">
			<div class="container">
				<div class="bg_black">
					
						<h1 class="page_title">				
						<span><?php the_field('banner_text'); ?></span>					
							
					</h1>
				</div>
			</div>
		</div>
	</div>

    <div class="container">
	<div class="top-six-box">
		<div class="row">
			<div class="col-md-2 col-sm-6">
				<div class="d-flex flex-column justify-content-between top_list">
					<div class="text-center">
						<span><?php echo get_field('box_1_title'); ?></span>
						<span class="text-subtitle"><?php echo get_field('box_1_subtitle'); ?></span>
					</div>
					<span style="height:6px;display:block;width:141px; background-color: <?php echo get_field('box_1_bg_color'); ?>"></span>
				</div>
			</div>
			<div class="col-md-2 col-sm-6">
				<div class="d-flex flex-column justify-content-between top_list">
					<div class="text-center">
						<span><?php echo get_field('box_2_title'); ?></span><br>
						<span class="text-subtitle"><?php echo get_field('box_2_subtitle'); ?></span>
					</div>
					<span style="height:6px;display:block;width:141px; background-color: <?php echo get_field('box_2_bg_color'); ?>"></span>
				</div>
			</div>
			<div class="col-md-2 col-sm-6">
				<div class="d-flex flex-column justify-content-between top_list">
					<div class="text-center">
						<span><?php echo get_field('box_3_title'); ?></span>
						<span class="text-subtitle"><?php echo get_field('box_3_subtitle'); ?></span>
					</div>
					<span style="height:6px;display:block;width:141px; background-color: <?php echo get_field('box_3_bg_color'); ?>"></span>
				</div>
			</div>
			<div class="col-md-2 col-sm-6">
				<div class="d-flex flex-column justify-content-between top_list">
					<div class="text-center">
						<span><?php echo get_field('box_4_title'); ?></span>
						<span class="text-subtitle"><?php echo get_field('box_4_subtitle'); ?></span>
					</div>
					<span style="height:6px;display:block;width:141px; background-color: <?php echo get_field('box_4_bg_color'); ?>"></span>
				</div>
			</div>
			<div class="col-md-2 col-sm-6">
				<div class="d-flex flex-column justify-content-between top_list">
					<div class="text-center">				
						<span><?php echo get_field('box_5_title'); ?></span>
						<span class="text-subtitle"><?php echo get_field('box_5_subtitle'); ?></span>
					</div>
					<span style="height:6px;display:block;width:141px; background-color: <?php echo get_field('box_5_bg_color'); ?>"></span>
				</div>
			</div>
			<div class="col-md-2 col-sm-6">
				<div class="d-flex flex-column justify-content-between top_list">
					<div class="text-center">				
						<span><?php echo get_field('box_6_title'); ?></span><br>
						<span class="text-subtitle"><?php echo get_field('box_6_subtitle'); ?></span>
					</div>
						<span style="height:6px;display:block;width:141px; background-color: <?php echo get_field('box_3_bg_color'); ?>"></span>
					</div>
			</div>
		</div>
	
	</div>
	</div>

	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="detail-wrap">
				<div class="entry-content">
					<?php
						the_content();

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-new-site' ),
							'after'  => '</div>',
						) );
					?>
				</div><!-- .entry-content -->
			</div>
		</article><!-- #post-## -->
	</div>
</div>
<?php
	get_footer();
?>