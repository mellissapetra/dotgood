<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dotgood
 */
get_header();
?>
<div class="main-container">
	<?php 
	$blog_id = get_option( 'page_for_posts' ); ?>
	<div <?php echo get_field('banner_image', $blog_id) ? 'class="page_banner"' : 'class="page_banner no_banner-image"'; ?>>
		<div class="banner-top">
			 <?php if( get_field('banner_image', $blog_id) ): ?>
			    <img src="<?php echo get_field('banner_image', $blog_id); ?>" />
			<?php endif; ?>
		</div>
		<div class="banner_text">
			<div class="container">
				<div class="bg_black">
					<h1 class="page_title">				
						<span><?php echo get_field('banner_text', $blog_id); ?></span>
					</h1>
				</div>
			</div>
		</div>
		</div>
		<div class="related_post-container">
			<div class="container">
				<div class="related_post">
					<div class="row">
						<div class="col-md-9">
								<div class="row">
									<?php
										if ( have_posts() ) :
									    if ( is_home() && ! is_front_page() ) : ?>
											<?php endif;
											while ( have_posts() ) : the_post();
											get_template_part( 'template-parts/content-blog' ); ?>
											<?php endwhile; ?>
										<?php get_template_part( 'template-parts/content', 'pagination' );
									else :
										get_template_part( 'template-parts/content', 'none' );
									endif; 
								?>
							</div>
						</div>
						<div class="col-md-3">
							<?php dynamic_sidebar('blog-right-side')?>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

<div class="blog-work-together">
	<?php echo get_the_content(null, false, $blog_id);?>

</div>

<?php
get_footer();
?>